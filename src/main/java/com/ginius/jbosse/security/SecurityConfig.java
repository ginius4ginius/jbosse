package com.ginius.jbosse.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.Md4PasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

import ch.qos.logback.classic.sift.MDCBasedDiscriminator;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	DataSource dataSource;

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.jdbcAuthentication().dataSource(dataSource)
				.usersByUsernameQuery(
						"select email as principal, password as credentials, active from user where email=?")
				.authoritiesByUsernameQuery("select email as principal, role as role from user where email=?")
				.rolePrefix("ROLE_").passwordEncoder(new BCryptPasswordEncoder());

	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.formLogin().loginPage("/login");
		http.authorizeRequests()
				.antMatchers("/index").permitAll()
				.antMatchers("/inscription").permitAll()
				.antMatchers("/anecdote").permitAll()
				.antMatchers("/contact").permitAll()
				.antMatchers("/newAnecdote").hasAnyRole("USER,ADMIN,MOD")
				.antMatchers("/admin/administration/**").access("hasRole('ADMIN')")
				.antMatchers("/admin/moderation/**").hasAnyRole("ADMIN,MOD");
		http.exceptionHandling().accessDeniedPage("/403");
	}

}
