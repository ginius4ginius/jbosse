package com.ginius.jbosse.security;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ginius.jbosse.persistance.entities.AnecdoteCategory;
import com.ginius.jbosse.service.JbosseService;

@Controller
public class SecurityController {

	@Autowired
	private JbosseService jbosseService;

	@RequestMapping(value = "/login")
	public String login(ModelMap map) {

		List<AnecdoteCategory> categoryList = jbosseService.anecdoteCategoryList();
		map.put("categoryList", categoryList);

		return "login";
	}

	@RequestMapping(value = "/")
	public String home() {
		return "redirect:/index";
	}

	@RequestMapping(value = "/403")
	public String accesDenied() {
		return "403";
	}

	@RequestMapping(value = "/logout")
	public String logout(HttpServletRequest request, HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}

		return "redirect:/index";
	}
}
