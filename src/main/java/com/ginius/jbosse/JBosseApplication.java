package com.ginius.jbosse;

import java.time.LocalDateTime;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.ginius.jbosse.persistance.dao.AnecdoteCategoryRepository;
import com.ginius.jbosse.persistance.dao.AnecdoteRepository;
import com.ginius.jbosse.persistance.dao.CommentRepository;
import com.ginius.jbosse.persistance.dao.CommentUsefullRepository;
import com.ginius.jbosse.persistance.dao.CommentUselessRepository;
import com.ginius.jbosse.persistance.dao.RolesRepository;
import com.ginius.jbosse.persistance.dao.StatusDesRepository;
import com.ginius.jbosse.persistance.dao.StatusFunRepository;
import com.ginius.jbosse.persistance.dao.StatusSadRepository;
import com.ginius.jbosse.persistance.dao.UserRepository;
import com.ginius.jbosse.persistance.entities.Anecdote;
import com.ginius.jbosse.persistance.entities.AnecdoteCategory;
import com.ginius.jbosse.persistance.entities.Comment;
import com.ginius.jbosse.persistance.entities.CommentUsefull;
import com.ginius.jbosse.persistance.entities.CommentUseless;
import com.ginius.jbosse.persistance.entities.Roles;
import com.ginius.jbosse.persistance.entities.Sexe;
import com.ginius.jbosse.persistance.entities.StatusDes;
import com.ginius.jbosse.persistance.entities.StatusFun;
import com.ginius.jbosse.persistance.entities.StatusSad;
import com.ginius.jbosse.persistance.entities.User;

@SpringBootApplication
public class JBosseApplication implements CommandLineRunner {

	@Autowired
	private AnecdoteCategoryRepository anecdoteCategoryRepository;

	@Autowired
	private AnecdoteRepository anecdoteRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CommentRepository commentRepository;

	@Autowired
	private StatusDesRepository statusDesRepository;

	@Autowired
	private StatusFunRepository statusFunRepository;

	@Autowired
	private StatusSadRepository statusSadRepository;

	@Autowired
	private CommentUsefullRepository commentUsefullRepository;

	@Autowired
	private CommentUselessRepository commentUselessRepository;
	
	
	@Autowired
	private RolesRepository rolesRepository;
	
	private static Logger LOG = Logger.getLogger(JBosseApplication.class);
	

	private static LocalDateTime date = LocalDateTime.now();

	public static void main(String[] args) {
		
		
		
		SpringApplication.run(JBosseApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		LOG.debug("Ceci est un message de débogage");
		LOG.info("Ceci est un message d'information");
		LOG.warn("Ceci est un message d'avertissement");
		LOG.error("Ceci est un message d'erreur");
		LOG.fatal("Ceci est un message d'erreur fatale");
		
		
		//creer les roles
		
//		Roles role1 = rolesRepository.save(new Roles("ADMIN"));
//		Roles role2 = rolesRepository.save(new Roles("MOD"));
//		Roles role3 = rolesRepository.save(new Roles("USER"));

		// creer utilisateurs
//		User user1 = userRepository.save(new User("ginius4@hotmail.com", "derieu", "vincent", "ginius",Sexe.Homme,new BCryptPasswordEncoder().encode("123456"),role3));
//		User user2 = userRepository.save(new User("derieu.vincent@outlook.com", "derieu", "dominique", "dom",Sexe.Homme,new BCryptPasswordEncoder().encode("123456"),role1));
//		User user3 = userRepository.save(new User("bouh20@hotmail.com", "derieu", "lise", "bouh20",Sexe.Femme,new BCryptPasswordEncoder().encode("123456"),role2));
//		
//		
		// creer les catégorie
//		AnecdoteCategory cat1 = anecdoteCategoryRepository.save(new AnecdoteCategory("artisanat"));
//		AnecdoteCategory cat2 = anecdoteCategoryRepository.save(new AnecdoteCategory("tourisme"));
//		AnecdoteCategory cat3 = anecdoteCategoryRepository.save(new AnecdoteCategory("éductation"));

		// creer anecdotes

//		Anecdote a1 = anecdoteRepository.save(new Anecdote(date, "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, sed quia nunc sine teste dico.", cat1, user1));
//		Anecdote a2 = anecdoteRepository.save(new Anecdote(date, "Ex turba vero imae sortis et paupertinae in tabernis aliqui pernoctant vinariis, non nulli velariis umbraculorum theatralium latent, quae Campanam imitatus lasciviam Catulus in aedilitate sua suspendit omnium primus; aut pugnaciter aleis certant turpi sono fragosis naribus introrsum reducto spiritu concrepantes; aut quod est studiorum omnium maximum ab ortu lucis ad vesperam sole fatiscunt vel pluviis, per minutias aurigarum equorumque praecipua vel delicta scrutantes.", cat2, user1));
//		Anecdote a3 = anecdoteRepository.save(new Anecdote(date, "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, sed quia nunc sine teste dico.", cat1, user2));
//		Anecdote a4 = anecdoteRepository.save(new Anecdote(date, "Ex turba vero imae sortis et paupertinae in tabernis aliqui pernoctant vinariis, non nulli velariis umbraculorum theatralium latent, quae Campanam imitatus lasciviam Catulus in aedilitate sua suspendit omnium primus; aut pugnaciter aleis certant turpi sono fragosis naribus introrsum reducto spiritu concrepantes; aut quod est studiorum omnium maximum ab ortu lucis ad vesperam sole fatiscunt vel pluviis, per minutias aurigarum equorumque praecipua vel delicta scrutantes.", cat3, user2));
//		Anecdote a5 = anecdoteRepository.save(new Anecdote(date, "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, sed quia nunc sine teste dico.", cat2, user3));
//		Anecdote a6 = anecdoteRepository.save(new Anecdote(date, "Ex turba vero imae sortis et paupertinae in tabernis aliqui pernoctant vinariis, non nulli velariis umbraculorum theatralium latent, quae Campanam imitatus lasciviam Catulus in aedilitate sua suspendit omnium primus; aut pugnaciter aleis certant turpi sono fragosis naribus introrsum reducto spiritu concrepantes; aut quod est studiorum omnium maximum ab ortu lucis ad vesperam sole fatiscunt vel pluviis, per minutias aurigarum equorumque praecipua vel delicta scrutantes.", cat3, user3));
//		Anecdote a7 = anecdoteRepository.save(new Anecdote(date, "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, sed quia nunc sine teste dico.", cat3, user3));
//		Anecdote a8 = anecdoteRepository.save(new Anecdote(date, "Ex turba vero imae sortis et paupertinae in tabernis aliqui pernoctant vinariis, non nulli velariis umbraculorum theatralium latent, quae Campanam imitatus lasciviam Catulus in aedilitate sua suspendit omnium primus; aut pugnaciter aleis certant turpi sono fragosis naribus introrsum reducto spiritu concrepantes; aut quod est studiorum omnium maximum ab ortu lucis ad vesperam sole fatiscunt vel pluviis, per minutias aurigarum equorumque praecipua vel delicta scrutantes.", cat3, user3));
//		Anecdote a9 = anecdoteRepository.save(new Anecdote(date, "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, sed quia nunc sine teste dico.", cat3, user3));
//		Anecdote a10 = anecdoteRepository.save(new Anecdote(date, "Ex turba vero imae sortis et paupertinae in tabernis aliqui pernoctant vinariis, non nulli velariis umbraculorum theatralium latent, quae Campanam imitatus lasciviam Catulus in aedilitate sua suspendit omnium primus; aut pugnaciter aleis certant turpi sono fragosis naribus introrsum reducto spiritu concrepantes; aut quod est studiorum omnium maximum ab ortu lucis ad vesperam sole fatiscunt vel pluviis, per minutias aurigarum equorumque praecipua vel delicta scrutantes.", cat3, user3));

		// commenter un nacdote

//		Comment c1 = commentRepository.save(new Comment("premier commentaire aneceote 1", date, a1, user2));
//		Comment c2 = commentRepository.save(new Comment("deuxième commentaire aneceote 1", date, a1, user3));
//
//		Comment c3 = commentRepository.save(new Comment("premier commentaire aneceote 2", date, a2, user2));
//
//		Comment c4 = commentRepository.save(new Comment("premier commentaire aneceote 3", date, a3, user3));
//
//		Comment c5 = commentRepository.save(new Comment("premier commentaire aneceote 4", date, a4, user3));
//
//		Comment c6 = commentRepository.save(new Comment("premier commentaire aneceote 5", date, a5, user2));
//
//		Comment c7 = commentRepository.save(new Comment("premier commentaire aneceote 6", date, a6, user1));
//		Comment c8 = commentRepository.save(new Comment("deuxième commentaire aneceote 6", date, a6, user2));

		// voter une anecdote

//		statusFunRepository.save(new StatusFun(a1));
//		statusFunRepository.save(new StatusFun(a2));
//		statusFunRepository.save(new StatusFun(a3));
//		statusFunRepository.save(new StatusFun(a4));
//
//		statusDesRepository.save(new StatusDes(a1));
//
//		statusSadRepository.save(new StatusSad(a2));

		// voter un commentaire
//
//		commentUsefullRepository.save(new CommentUsefull(c1));
//		commentUsefullRepository.save(new CommentUsefull(c1));
//
//		commentUsefullRepository.save(new CommentUsefull(c2));
//		commentUselessRepository.save(new CommentUseless(c2));
//
//		commentUsefullRepository.save(new CommentUsefull(c3));
//		commentUselessRepository.save(new CommentUseless(c3));
//
//		commentUsefullRepository.save(new CommentUsefull(c4));
//		commentUselessRepository.save(new CommentUseless(c4));
//
//		commentUsefullRepository.save(new CommentUsefull(c5));
//		commentUselessRepository.save(new CommentUseless(c5));
//
//		commentUsefullRepository.save(new CommentUsefull(c6));
//		commentUselessRepository.save(new CommentUseless(c6));
//
//		commentUsefullRepository.save(new CommentUsefull(c7));
//		commentUselessRepository.save(new CommentUseless(c7));
//
//		commentUsefullRepository.save(new CommentUsefull(c8));
//		commentUselessRepository.save(new CommentUseless(c8));

	}
}
