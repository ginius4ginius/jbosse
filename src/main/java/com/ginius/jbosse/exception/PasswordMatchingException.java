package com.ginius.jbosse.exception;

public class PasswordMatchingException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PasswordMatchingException() {
		super();
	}

	public PasswordMatchingException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public PasswordMatchingException(final String message) {
		super(message);
	}

	public PasswordMatchingException(final Throwable cause) {
		super(cause);
	}

}
