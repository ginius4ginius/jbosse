package com.ginius.jbosse.exception;

public class EmailMatchingException  extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public EmailMatchingException() {
		super();
	}
	
	public EmailMatchingException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public EmailMatchingException(final String message) {
        super(message);
    }

    public EmailMatchingException(final Throwable cause) {
        super(cause);
    }

}
