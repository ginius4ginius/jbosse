package com.ginius.jbosse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ginius.jbosse.exception.EmailExistsException;
import com.ginius.jbosse.exception.EmailMatchingException;
import com.ginius.jbosse.exception.PasswordMatchingException;
import com.ginius.jbosse.persistance.dao.UserRepository;
import com.ginius.jbosse.persistance.entities.Roles;
import com.ginius.jbosse.persistance.entities.Sexe;
import com.ginius.jbosse.persistance.entities.User;

@Service
@Transactional
public class UserService implements IUserService {

	// instances
	@Autowired
	private UserRepository userRepository;

	// méthodes

	/**
	 * méthode pour créer un utilisateur
	 */
	@Override
	public User newUser(String email, String lastName, String firstName, String login, Sexe sexe, String password,
			Roles role) {
		return new User(email, lastName, firstName, login, sexe, password, role);
	}

	@Override
	public User userRegistration(User user) {
		User newUser = new User();
		
		String pswdEnc = new BCryptPasswordEncoder().encode(user.getMatchingPassword());

		newUser.setEmail(user.getEmail());
		newUser.setFirstName(user.getFirstName());
		newUser.setLastName(user.getLastName());
		newUser.setLogin(user.getLogin());
		newUser.setPassword(pswdEnc);
		newUser.setActive(true);
		newUser.setSexe(user.getSexe());
		newUser.setRole(new Roles("USER"));

		return userRepository.save(newUser);

	}

	@Override
	public User registerNewUserAccount(User account)
			throws EmailExistsException, EmailMatchingException, PasswordMatchingException {

		if (emailExist(account.getEmail())) {
			throw new EmailExistsException("Un compte existe déjà avec cet email : " + account.getEmail());

		}
		if (emailWrongMatching(account.getEmail(), account.getMatchingEmail())) {
			throw new EmailMatchingException(account.getEmail() + " doit être inscrit dans le champ.");

		}
		if (passwordWrongMatching(account.getPassword(), account.getMatchingPassword())) {
			throw new PasswordMatchingException("Les mots de passe ne sont pas identique");

		}
		return userRegistration(account);

	}

	@Override
	public boolean emailExist(String email) {
		return userRepository.findByEmail(email) != null;
	}

	@Override
	public boolean emailWrongMatching(String email, String matchingEmail) {
		if (email.equals(matchingEmail)) {
			return false;
		}
		return true;
	}

	@Override
	public boolean passwordWrongMatching(String password, String matchingPassword) {
		if (password.equals(matchingPassword)) {
			return false;
		}
		return true;
	}

}
