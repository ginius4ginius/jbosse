package com.ginius.jbosse.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ginius.jbosse.persistance.dao.AnecdoteCategoryRepository;
import com.ginius.jbosse.persistance.dao.AnecdoteRepository;
import com.ginius.jbosse.persistance.dao.CommentRepository;
import com.ginius.jbosse.persistance.dao.CommentUsefullRepository;
import com.ginius.jbosse.persistance.dao.CommentUselessRepository;
import com.ginius.jbosse.persistance.dao.StatusDesRepository;
import com.ginius.jbosse.persistance.dao.StatusFunRepository;
import com.ginius.jbosse.persistance.dao.StatusSadRepository;
import com.ginius.jbosse.persistance.entities.Anecdote;
import com.ginius.jbosse.persistance.entities.AnecdoteCategory;
import com.ginius.jbosse.persistance.entities.Comment;
import com.ginius.jbosse.persistance.entities.CommentUsefull;
import com.ginius.jbosse.persistance.entities.CommentUseless;
import com.ginius.jbosse.persistance.entities.Roles;
import com.ginius.jbosse.persistance.entities.StatusDes;
import com.ginius.jbosse.persistance.entities.StatusFun;
import com.ginius.jbosse.persistance.entities.StatusSad;
import com.ginius.jbosse.persistance.entities.User;

@Service
@Transactional
public class JbosseService implements IJbosseService {

	// instances

	@Autowired
	private AnecdoteRepository anecdoteRepository;

	@Autowired
	private AnecdoteCategoryRepository anecdoteCategoryRepository;

	@Autowired
	private CommentUsefullRepository commentUsefullRepository;

	@Autowired
	private CommentUselessRepository commentUselessRepository;

	@Autowired
	private CommentRepository commentRepository;

	@Autowired
	private StatusDesRepository statusDesRepository;

	@Autowired
	private StatusFunRepository statusFunRepository;

	@Autowired
	private StatusSadRepository statusSadRepository;
	
	
	// méthodes

	/**
	 * retourne en page les 10 meilleures anecdotes
	 */
	@Override
	public List<Anecdote> bestAnecdoteList() {

		return anecdoteRepository.bestAnecdoteList();
	}

	/**
	 * retourne en page les 10 dernières anecdotes
	 */
	@Override
	public List<Anecdote> lastAnecdoteList() {
		return anecdoteRepository.lastAnecdoteList();
	}

	/**
	 * retourne en page la liste des anecdotes pas catégorie
	 */
	@Override
	public Page<Anecdote> anecdoteByCategoryList(int categoryId, int page, int size) {
		return anecdoteRepository.anecdoteByCategoryList(categoryId, new PageRequest(page, size));

	}

	/**
	 * retourne la liste de catégorie
	 */
	@Override
	public List<AnecdoteCategory> anecdoteCategoryList() {

		return anecdoteCategoryRepository.findAll();
	}

	/**
	 * retourne en page la liste des commentaires d'une anecdote
	 */
	@Override
	public Page<Comment> commentsList(int anecdoteId, int page, int size) {

		return commentRepository.commentsList(anecdoteId, new PageRequest(page, size));
	}

	

	/**
	 * méthode pour créer une catégorie
	 */
	@Override
	public AnecdoteCategory newAnecdoteCategory(String label) {

		return new AnecdoteCategory(label);
	}

	/**
	 * méthode pour créer une anecdote
	 */
	@Override
	public Anecdote newAnecdote(LocalDateTime publishedAt, String description, AnecdoteCategory category, User user) {

		return new Anecdote(publishedAt, description, category, user);
	}

	/**
	 * Permet de voter une anecdote comme "Tu l'as bie nmérité"
	 */
	@Override
	public void voteAnecdoteDes(StatusDes status) {
		statusDesRepository.save(status);

	}

	/**
	 * Permet de voter une anecdote comme "drole"
	 */
	@Override
	public void voteAnecdoteFun(StatusFun status) {
		statusFunRepository.save(status);

	}

	/**
	 * Permet de voter une anecdote comme "Tu l'as bien mérité"
	 */
	@Override
	public void voteAnecdoteSad(StatusSad status) {
		statusSadRepository.save(status);

	}

	/**
	 * méthode pour créer un commentaire
	 */
	@Override
	public Comment NewComment(String description, LocalDateTime publishedAt, Anecdote anecdote, User user) {

		return new Comment(description, publishedAt, anecdote, user);
	}

	/**
	 * Permet de voter un commentaire comme "Utile"
	 */
	@Override
	public void voteCommentUsefull(CommentUsefull comment) {

		commentUsefullRepository.save(comment);

	}

	/**
	 * Permet de voter un commentaire comme "Inutile"
	 */
	@Override
	public void voteCommentUseless(CommentUseless comment) {

		commentUselessRepository.save(comment);

	}

	/**
	 * Méthode qui récupère une anecote par son ID
	 */
	@Override
	public Anecdote FindAnecdoteById(int anecdoteId) {

		Anecdote anecdote = anecdoteRepository.findById(anecdoteId).orElse(null);

		if (anecdote == null)
			throw new RuntimeException("L'anecdote n'exsiste pas");
		return anecdote;
	}

	/**
	 * méthode pour créer un role
	 * 
	 * @return
	 */
	@Override
	public Roles newRole(String role) {

		return new Roles(role);

	}


}
