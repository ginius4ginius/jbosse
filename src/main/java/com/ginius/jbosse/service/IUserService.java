package com.ginius.jbosse.service;

import com.ginius.jbosse.persistance.entities.Roles;
import com.ginius.jbosse.persistance.entities.Sexe;
import com.ginius.jbosse.persistance.entities.User;

public interface IUserService {

	// lists

	// create

	public User newUser(String email, String lastName, String firstName, String login, Sexe sexe, String password,
			Roles role);

	public User userRegistration(User user);

	public User registerNewUserAccount(User account);

	// consult

	public boolean emailExist(String email);

	public boolean emailWrongMatching(String email, String matchingEmail);

	public boolean passwordWrongMatching(String password, String matchingMAssword);

}
