package com.ginius.jbosse.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;

import com.ginius.jbosse.persistance.entities.Anecdote;
import com.ginius.jbosse.persistance.entities.AnecdoteCategory;
import com.ginius.jbosse.persistance.entities.Comment;
import com.ginius.jbosse.persistance.entities.CommentUsefull;
import com.ginius.jbosse.persistance.entities.CommentUseless;
import com.ginius.jbosse.persistance.entities.Roles;
import com.ginius.jbosse.persistance.entities.StatusDes;
import com.ginius.jbosse.persistance.entities.StatusFun;
import com.ginius.jbosse.persistance.entities.StatusSad;
import com.ginius.jbosse.persistance.entities.User;

public interface IJbosseService {

	// Lists

	public List<Anecdote> bestAnecdoteList();

	public List<Anecdote> lastAnecdoteList();

	public Page<Anecdote> anecdoteByCategoryList(int categoryId, int page, int size);

	public List<AnecdoteCategory> anecdoteCategoryList();

	public Page<Comment> commentsList(int anecdoteId, int page, int size);

	// create

	

	public AnecdoteCategory newAnecdoteCategory(String label);

	public Anecdote newAnecdote(LocalDateTime publishedAt, String description, AnecdoteCategory category, User user);

	public void voteAnecdoteDes(StatusDes status);

	public void voteAnecdoteFun(StatusFun status);

	public void voteAnecdoteSad(StatusSad status);

	public Comment NewComment(String description, LocalDateTime publishedAt, Anecdote anecdote, User user);

	public void voteCommentUsefull(CommentUsefull comment);

	public void voteCommentUseless(CommentUseless comment);
	
	public Roles newRole(String role);
	

	// consult

	public Anecdote FindAnecdoteById(int anecdoteId);
	
	

	

}
