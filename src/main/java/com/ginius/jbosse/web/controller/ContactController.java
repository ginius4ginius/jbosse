package com.ginius.jbosse.web.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ginius.jbosse.persistance.entities.AnecdoteCategory;
import com.ginius.jbosse.service.JbosseService;

@Controller
public class ContactController {
	
	@Autowired
	JbosseService jbosseService;
	
	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	public String getContact(HttpSession session, ModelMap map) {

		List<AnecdoteCategory> categoryList = jbosseService.anecdoteCategoryList();
		String user = session.getId();// déclaration d'une session.
		map.put("session", user);
		map.put("categoryList", categoryList);

		return "contact";
	}
	
	

}
