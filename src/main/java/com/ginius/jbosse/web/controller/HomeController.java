package com.ginius.jbosse.web.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ginius.jbosse.persistance.entities.Anecdote;
import com.ginius.jbosse.persistance.entities.AnecdoteCategory;
import com.ginius.jbosse.service.JbosseService;

@Controller
public class HomeController {
	
	@Autowired
	private JbosseService jbosseService;

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String getHome(HttpSession session, ModelMap map) {

		String user = session.getId();// déclaration d'une session.
		List<AnecdoteCategory> categoryList = jbosseService.anecdoteCategoryList();
		List<Anecdote> lastAnecdote = jbosseService.lastAnecdoteList();
		List<Anecdote> bestAnecdote = jbosseService.bestAnecdoteList();

		// map.put("userSession", user);//insersion de la session dans le Modelmap
		// inséré dans la vue.
		map.put("categoryList", categoryList);
		map.put("lastAnecdote", lastAnecdote);
		map.put("bestAnecdote", bestAnecdote);
		map.put("session", user);

		return "accueil";
	}

}
