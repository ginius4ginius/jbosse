package com.ginius.jbosse.web.controller;

import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

import java.util.List;

import javax.validation.Valid;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.apache.log4j.Logger;

import com.ginius.jbosse.exception.EmailExistsException;
import com.ginius.jbosse.exception.EmailMatchingException;
import com.ginius.jbosse.exception.PasswordMatchingException;
import com.ginius.jbosse.persistance.entities.Anecdote;
import com.ginius.jbosse.persistance.entities.AnecdoteCategory;
import com.ginius.jbosse.persistance.entities.User;
import com.ginius.jbosse.service.JbosseService;
import com.ginius.jbosse.service.UserService;


@Controller
@RequestMapping(value = "/user/inscription")
public class InscriptionController {

	private static Logger LOGGER = Logger.getLogger(InscriptionController.class);
	
	@Autowired
	private JbosseService jbosseService;

	@Autowired
	private UserService userService;

	/**
	 * méthode qui charge la vue de l'inscription
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String showInscriptionForm(final HttpServletRequest request, final Model model) {

		List<AnecdoteCategory> categoryList = jbosseService.anecdoteCategoryList();
		final User user = new User();

		String[] genre = { "homme", "femme" };
		model.addAttribute("categoryList", categoryList);
		model.addAttribute("genres", genre);
		model.addAttribute("user", user);

		return "inscription";
	}

	/**
	 * méthode qui valide le processus de persistance de l'utilisateur dans la base
	 * de données.
	 * 
	 * @param account
	 * @param result
	 * @param request
	 * @param errors
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public String registerUserAcount(@ModelAttribute("user") @Valid final User user, BindingResult result,
			final HttpServletRequest request, HttpSession session, final Errors errors, ModelMap map) {
		
		LOGGER.info("RUNNING - Enregistrement d'un compte utilisateur avec les informations: "+user);
		
		User registered = null;
		//Map <String, Object> map = new HashMap<String,Object>();
		
		try {
			registered = userService.registerNewUserAccount(user);
		} catch (EmailExistsException ex) {
			LOGGER.warn("ERROR - Impossible d'enregistrer l'utilisateur Email déja dans la base de donnée: Un compte existe déjà avec cet email :"+user.getEmail());
			String message = ex.getMessage();
			map.put("ExceptionEmailExist", message);
			map.put("user", user);
			return "inscription";
			
		}catch (EmailMatchingException ex) {
			LOGGER.warn("ERROR - Les emails ne sont pas identiques: "+user.getEmail());
			String message = ex.getMessage();
			map.put("ExceptionEmailMatching", message);
			map.put("user", user);
			return "inscription";
			
		}catch (PasswordMatchingException ex) {
			LOGGER.warn("ERROR - Les mots de passe ne sont pas identiques.");
			String message = ex.getMessage();
			map.put("ExceptionPasswordMatching", message);
			map.put("user", user);
			return "inscription";
			
		}catch (Exception e) {
			LOGGER.warn("ERROR - erreur lors de l'insersion de l'utilisateur dans la base de donnée: ");
			map.put("user", user);
			return "inscription";
		}
		
		if (registered == null) {
			return "inscription";

		} else {
			
			LOGGER.info("SUCCES -Insersion de l'utilisateur "+user.getEmail()+" dans la base de donnée réussit");
			
	
			String userSession = session.getId();// déclaration d'une session.
			List<AnecdoteCategory> categoryList = jbosseService.anecdoteCategoryList();
			List<Anecdote> lastAnecdote = jbosseService.lastAnecdoteList();
			List<Anecdote> bestAnecdote = jbosseService.bestAnecdoteList();
			String message = "Merci pour ton inscription "+user.getFirstName()+ " Il ne reste plus qu'à te connecter pour poster tes anecdotes.";
			// map.put("userSession", user);//insersion de la session dans le Modelmap
			// inséré dans la vue.
			map.put("categoryList", categoryList);
			map.put("lastAnecdote", lastAnecdote);
			map.put("bestAnecdote", bestAnecdote);
			map.put("session", userSession);
			map.put("successMessage", message);

			return "accueil";
		}
	}

}
