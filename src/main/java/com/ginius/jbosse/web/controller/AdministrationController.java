package com.ginius.jbosse.web.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ginius.jbosse.persistance.entities.AnecdoteCategory;
import com.ginius.jbosse.service.JbosseService;

@Controller
public class AdministrationController {
	
	@Autowired
	JbosseService jbosseService;

	@RequestMapping(value = "/admin/administration", method = RequestMethod.GET)
	public String getAdmin(HttpSession session, ModelMap map) {

		List<AnecdoteCategory> categoryList = jbosseService.anecdoteCategoryList();
		String user = session.getId();// déclaration d'une session.
		map.put("categoryList", categoryList);
		map.put("session", user);

		return "administration";
	}
	
	@RequestMapping(value = "/admin/moderation", method = RequestMethod.GET)
	public String getMod(HttpSession session, ModelMap map) {

		List<AnecdoteCategory> categoryList = jbosseService.anecdoteCategoryList();
		String user = session.getId();// déclaration d'une session.
		map.put("categoryList", categoryList);
		map.put("session", user);

		return "moderation";
	}

}
