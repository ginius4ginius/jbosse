package com.ginius.jbosse.web.dto;

import java.io.Serializable;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.ginius.jbosse.persistance.entities.Sexe;
import com.ginius.jbosse.validation.annotations.EmailMatches;
import com.ginius.jbosse.validation.annotations.PasswordMatches;
import com.ginius.jbosse.validation.annotations.ValidEmail;

//	@EmailMatches
//	@PasswordMatches
	public class UserDto implements Serializable {

		private static final long serialVersionUID = 1L;

		@NotEmpty(message = "Le champs ne peut être vide")
		//@ValidEmail
		//@Size(min = 1, message = "{Size.user.email}")
		private String email;
		
		@NotEmpty(message = "Le champs ne peut être vide")
		//@Size(min = 1, message = "{Size.user.matchingEmail}")
		private String matchingEmail;
		
		
		@Enumerated(EnumType.STRING)
		private Sexe sexe;

		@NotEmpty(message = "Le champs ne peut être vide")
		@Size(min = 2, max = 20, message = "{Size.user.lastName}")
		private String lastName;

		@NotEmpty(message = "Le champs ne peut être vide")
		@Size(min = 2, max = 20, message = "{Size.user.firstName}")
		private String firstName;

		@NotEmpty(message = "Le champs ne peut être vide")
		@Size(min = 2, max = 20, message = "{Size.user.login}")
		private String login;  
		
		@NotEmpty(message = "Le champs ne peut être vide")
		@Size(min = 6, max = 20 , message = "{Size.user.password}")
		private String password;
		
		@NotEmpty(message = "Le champs ne peut être vide")
		@Size(min = 6, max = 20 , message = "{Size.user.matchingPassword}")
		private String matchingPassword;
		
		public UserDto() {

		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLogin() {
			return login;
		}

		public void setLogin(String login) {
			this.login = login;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getMatchingEmail() {
			return matchingEmail;
		}

		public void setMatchingEmail(String matchingEmail) {
			this.matchingEmail = matchingEmail;
		}

		public String getMatchingPassword() {
			return matchingPassword;
		}

		public void setMatchingPassword(String matchingPassword) {
			this.matchingPassword = matchingPassword;
		}

		public Sexe getSexe() {
			return sexe;
		}

		public void setSexe(Sexe sexe) {
			this.sexe = sexe;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((email == null) ? 0 : email.hashCode());
			result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
			result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
			result = prime * result + ((login == null) ? 0 : login.hashCode());
			result = prime * result + ((matchingEmail == null) ? 0 : matchingEmail.hashCode());
			result = prime * result + ((matchingPassword == null) ? 0 : matchingPassword.hashCode());
			result = prime * result + ((password == null) ? 0 : password.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			UserDto other = (UserDto) obj;
			if (email == null) {
				if (other.email != null)
					return false;
			} else if (!email.equals(other.email))
				return false;
			if (firstName == null) {
				if (other.firstName != null)
					return false;
			} else if (!firstName.equals(other.firstName))
				return false;
			if (lastName == null) {
				if (other.lastName != null)
					return false;
			} else if (!lastName.equals(other.lastName))
				return false;
			if (login == null) {
				if (other.login != null)
					return false;
			} else if (!login.equals(other.login))
				return false;
			if (matchingEmail == null) {
				if (other.matchingEmail != null)
					return false;
			} else if (!matchingEmail.equals(other.matchingEmail))
				return false;
			if (matchingPassword == null) {
				if (other.matchingPassword != null)
					return false;
			} else if (!matchingPassword.equals(other.matchingPassword))
				return false;
			if (password == null) {
				if (other.password != null)
					return false;
			} else if (!password.equals(other.password))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "UserDto [email=" + email + ", matchingEmail=" + matchingEmail + ", lastName=" + lastName
					+ ", firstName=" + firstName + ", login=" + login + ", password=" + password + ", matchingPassword="
					+ matchingPassword + "]";
		}

		

	}
