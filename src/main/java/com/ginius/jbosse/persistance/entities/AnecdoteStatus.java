package com.ginius.jbosse.persistance.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Classe abstraite représentant la table anecdote_status de la SGBDR
 * 
 * @author Derieu
 *
 */
@Entity
@Table(name="anecdote_status")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "STATUS_ANECDOTE",
discriminatorType = DiscriminatorType.STRING,
length = 3) // name="dType" DiscriminatorType.VARCHAR lenght=155 par defaut
public abstract class AnecdoteStatus implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "anecdote_status_id")
	private int id;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "anecdote_id", foreignKey = @ForeignKey(name = "fk_anecdote_status_anecdote"))
	private Anecdote anecdote;
	
	//constructeur
	
	public AnecdoteStatus() {
		
	}

	/**
	 * @param anecdote
	 */
	public AnecdoteStatus(Anecdote anecdote) {
		super();
		this.anecdote = anecdote;
	}

	public int getId() {
		return id;
	}

	public Anecdote getAnecdote() {
		return anecdote;
	}

	public void setAnecdote(Anecdote anecdote) {
		this.anecdote = anecdote;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((anecdote == null) ? 0 : anecdote.hashCode());
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnecdoteStatus other = (AnecdoteStatus) obj;
		if (anecdote == null) {
			if (other.anecdote != null)
				return false;
		} else if (!anecdote.equals(other.anecdote))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AnecdoteStatus [id=" + id + ", anecdote=" + anecdote + "]";
	}

	
	
}
