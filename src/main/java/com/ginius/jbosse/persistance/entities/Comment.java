package com.ginius.jbosse.persistance.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Classe  représentant la table comment de la SGBDR
 * 
 * @author Derieu
 *
 */
@Entity
@Table(name = "comment")
public class Comment implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "comment_id")
	private int id;

	@NotNull
	@Column(name = "description")
	private String description;

	@NotNull
	@JsonFormat(pattern = "dd-MM-yyyy hh:mm:ss")
	@DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
	@Column(name = "published_at")
	private LocalDateTime publishedAt;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "anecdote_id", foreignKey = @ForeignKey(name = "fk_comment_anecdote"))
	private Anecdote anecdote;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "email", foreignKey = @ForeignKey(name = "fk_comment_user"))
	private User user;

	@OneToMany(mappedBy = "comment", fetch = FetchType.LAZY)
	private Collection<CommentStatus> Status;

	// constructeur

	public Comment() {

	}

	/**
	 * @param description
	 * @param publishedAt
	 * @param anecdote
	 * @param user
	 */
	public Comment(String description, LocalDateTime publishedAt, Anecdote anecdote, User user) {
		super();
		this.description = description;
		this.publishedAt = publishedAt;
		this.anecdote = anecdote;
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getPublishedAt() {
		return publishedAt;
	}

	public void setPublishedAt(LocalDateTime publishedAt) {
		this.publishedAt = publishedAt;
	}

	public Anecdote getAnecdote() {
		return anecdote;
	}

	public void setAnecdote(Anecdote anecdote) {
		this.anecdote = anecdote;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Collection<CommentStatus> getStatus() {
		return Status;
	}

	public void setStatus(Collection<CommentStatus> status) {
		Status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Status == null) ? 0 : Status.hashCode());
		result = prime * result + ((anecdote == null) ? 0 : anecdote.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		result = prime * result + ((publishedAt == null) ? 0 : publishedAt.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (Status == null) {
			if (other.Status != null)
				return false;
		} else if (!Status.equals(other.Status))
			return false;
		if (anecdote == null) {
			if (other.anecdote != null)
				return false;
		} else if (!anecdote.equals(other.anecdote))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (publishedAt == null) {
			if (other.publishedAt != null)
				return false;
		} else if (!publishedAt.equals(other.publishedAt))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", description=" + description + ", publishedAt=" + publishedAt + ", anecdote="
				+ anecdote + ", user=" + user + ", Status=" + Status + "]";
	}

}
