package com.ginius.jbosse.persistance.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.ginius.jbosse.validation.annotations.EmailMatches;
import com.ginius.jbosse.validation.annotations.PasswordMatches;
import com.ginius.jbosse.validation.annotations.ValidEmail;

/**
 * Classe représentant la table user de la SGBDR
 * 
 * @author Derieu
 *
 */
@Entity
@Table(name = "user")
//@EmailMatches
//@PasswordMatches
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	
	@ValidEmail
	@Id
	@Column(name = "email", length = 50)
	//@NotEmpty(message = "Le champs ne peut être vide")
	private String email;

	@Column(name = "sexe", length = 5)
	@Enumerated(EnumType.STRING)
	private Sexe sexe;

	@NotEmpty(message = "Le champs ne peut être vide")
	@Size(min = 2, max = 20, message = "{Size.user.lastName}")
	@Column(name = "lastName", length = 20)
	private String lastName;

	@NotEmpty(message = "Le champs ne peut être vide")
	@Size(min = 2, max = 20, message = "{Size.user.firstName}")
	@Column(name = "firstName", length = 20)
	private String firstName;

	@NotEmpty(message = "Le champs ne peut être vide")
	@Size(min = 2, max = 20, message = "{Size.user.login}")
	@Column(name = "login", length = 20)
	private String login;

	@NotEmpty(message = "Le champs ne peut être vide")
	@Size(min = 6, max = 200 , message = "{Size.user.password}")
	@Column(name = "password", length = 200)
	private String password;

	@Column(name = "active")
	private boolean active = false;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private Collection<Anecdote> Anecdotes;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private Collection<Comment> Comments;

	@ManyToOne
	@JoinColumn(name = "role", foreignKey = @ForeignKey(name = "fk_role_user"))
	private Roles role;
	
	@Transient
	private String matchingPassword;
	
	@Transient
	private String matchingEmail;
	
	public User() {

	}

	/**
	 * @param email
	 * @param lastName
	 * @param firstName
	 * @param login
	 * @param birthDate
	 * @param sex
	 */
	public User(String email, String lastName, String firstName, String login, Sexe sexe, String password, Roles role) {
		super();
		this.email = email;
		this.lastName = lastName;
		this.firstName = firstName;
		this.login = login;
		this.sexe = sexe;
		this.password = password;
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Sexe getSexe() {
		return sexe;
	}

	public void setSexe(Sexe sexe) {
		this.sexe = sexe;
	}

	public Collection<Anecdote> getAnecdotes() {
		return Anecdotes;
	}

	public void setAnecdotes(Collection<Anecdote> anecdotes) {
		Anecdotes = anecdotes;
	}

	public Collection<Comment> getComments() {
		return Comments;
	}

	public void setComments(Collection<Comment> comments) {
		Comments = comments;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Roles getRole() {
		return role;
	}

	public void setRole(Roles role) {
		this.role = role;
	}

	public String getMatchingPassword() {
		return matchingPassword;
	}

	public void setMatchingPassword(String matchingPassword) {
		this.matchingPassword = matchingPassword;
	}

	public String getMatchingEmail() {
		return matchingEmail;
	}

	public void setMatchingEmail(String matchingEmail) {
		this.matchingEmail = matchingEmail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Anecdotes == null) ? 0 : Anecdotes.hashCode());
		result = prime * result + ((Comments == null) ? 0 : Comments.hashCode());
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((matchingEmail == null) ? 0 : matchingEmail.hashCode());
		result = prime * result + ((matchingPassword == null) ? 0 : matchingPassword.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((sexe == null) ? 0 : sexe.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (Anecdotes == null) {
			if (other.Anecdotes != null)
				return false;
		} else if (!Anecdotes.equals(other.Anecdotes))
			return false;
		if (Comments == null) {
			if (other.Comments != null)
				return false;
		} else if (!Comments.equals(other.Comments))
			return false;
		if (active != other.active)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (matchingEmail == null) {
			if (other.matchingEmail != null)
				return false;
		} else if (!matchingEmail.equals(other.matchingEmail))
			return false;
		if (matchingPassword == null) {
			if (other.matchingPassword != null)
				return false;
		} else if (!matchingPassword.equals(other.matchingPassword))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		if (sexe != other.sexe)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [email=" + email + ", sexe=" + sexe + ", lastName=" + lastName + ", firstName=" + firstName
				+ ", login=" + login + ", password=" + password + ", active=" + active + ", Anecdotes=" + Anecdotes
				+ ", Comments=" + Comments + ", role=" + role + ", matchingPassword=" + matchingPassword
				+ ", matchingEmail=" + matchingEmail + "]";
	}
	
	
}
