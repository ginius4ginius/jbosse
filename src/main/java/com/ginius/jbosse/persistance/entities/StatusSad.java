package com.ginius.jbosse.persistance.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Classe héritant de la classe abstraite AnecdoteStatus
 * 
 * @author Derieu
 *
 */
@Entity
@DiscriminatorValue("SAD")
public class StatusSad extends AnecdoteStatus {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public StatusSad() {
	}

	/**
	 * @param anecdote
	 */
	public StatusSad(Anecdote anecdote) {
		super(anecdote);
	}

}
