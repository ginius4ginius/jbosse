package com.ginius.jbosse.persistance.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Classe héritant de la classe abstraite AnecdoteStatus
 * 
 * @author Derieu
 *
 */
@Entity
@DiscriminatorValue("DES")
public class StatusDes extends AnecdoteStatus {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public StatusDes() {
	}

	/**
	 * @param anecdote
	 */
	public StatusDes(Anecdote anecdote) {
		super(anecdote);
	}

}
