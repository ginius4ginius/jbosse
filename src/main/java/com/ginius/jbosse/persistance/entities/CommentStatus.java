package com.ginius.jbosse.persistance.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Classe abstraite représentant la table comment_status de la SGBDR
 * 
 * @author Derieu
 *
 */
@Entity
@Table(name="comment_status")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "STATUS_COMMENT",
discriminatorType = DiscriminatorType.STRING,
length = 7) // name="dType" DiscriminatorType.VARCHAR lenght=155 par defaut
public abstract class CommentStatus implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "comment_status_id")
	private int id;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "comment_id", foreignKey = @ForeignKey(name = "fk_comment_status_comment"))
	private Comment comment;
	
	//constructeur
	
	public CommentStatus() {
		
	}

	/**
	 * @param comment
	 */
	public CommentStatus(Comment comment) {
		super();
		this.comment = comment;
	}

	public int getId() {
		return id;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommentStatus other = (CommentStatus) obj;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CommentStatus [id=" + id + ", comment=" + comment + "]";
	}

	
	

}
