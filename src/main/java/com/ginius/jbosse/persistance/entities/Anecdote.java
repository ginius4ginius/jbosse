package com.ginius.jbosse.persistance.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Classe représentant la table anecdote de la SGCDR
 * 
 * @author Derieu
 *
 */
@Entity
@Table(name = "anecdote")
public class Anecdote implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "anecdote_id")
	private int id;
	
	@NotNull
	@JsonFormat(pattern = "dd-MM-yyyy hh:mm:ss")
    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
	@Column(name = "published_at")
	private LocalDateTime publishedAt;
	
	@NotNull
	@Column(name = "description", length=500)
	private String description;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "category_id", foreignKey = @ForeignKey(name = "fk_anecdote_anecdote_category"))
	private AnecdoteCategory category;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "user_email", foreignKey = @ForeignKey(name = "fk_anecdote_user"))
	private User user;
	
	@OneToMany(mappedBy = "anecdote", fetch = FetchType.LAZY)
	private Collection<Comment> Comments;
	
	@OneToMany(mappedBy = "anecdote", fetch = FetchType.LAZY)
	private Collection<AnecdoteStatus> Status;
	
	//constructeur 
	
	public Anecdote() {
		
	}

	/**
	 * @param id
	 * @param publishedAt
	 * @param description
	 * @param category
	 * @param user
	 */
	public Anecdote(LocalDateTime publishedAt, String description, AnecdoteCategory category, User user) {
		this.publishedAt = publishedAt;
		this.description = description;
		this.category = category;
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public LocalDateTime getPublishedAt() {
		return publishedAt;
	}

	public void setPublishedAt(LocalDateTime publishedAt) {
		this.publishedAt = publishedAt;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public AnecdoteCategory getCategory() {
		return category;
	}

	public void setCategory(AnecdoteCategory category) {
		this.category = category;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Collection<Comment> getComments() {
		return Comments;
	}

	public void setComments(Collection<Comment> comments) {
		Comments = comments;
	}

	public Collection<AnecdoteStatus> getStatus() {
		return Status;
	}

	public void setStatus(Collection<AnecdoteStatus> status) {
		Status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Comments == null) ? 0 : Comments.hashCode());
		result = prime * result + ((Status == null) ? 0 : Status.hashCode());
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		result = prime * result + ((publishedAt == null) ? 0 : publishedAt.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Anecdote other = (Anecdote) obj;
		if (Comments == null) {
			if (other.Comments != null)
				return false;
		} else if (!Comments.equals(other.Comments))
			return false;
		if (Status == null) {
			if (other.Status != null)
				return false;
		} else if (!Status.equals(other.Status))
			return false;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (publishedAt == null) {
			if (other.publishedAt != null)
				return false;
		} else if (!publishedAt.equals(other.publishedAt))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Anecdote [id=" + id + ", publishedAt=" + publishedAt + ", description=" + description + ", category="
				+ category + ", user=" + user + ", Comments=" + Comments + ", Status=" + Status + "]";
	}
	
	
	
	
	
	
}
