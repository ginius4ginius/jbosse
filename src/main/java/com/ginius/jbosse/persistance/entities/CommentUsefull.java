package com.ginius.jbosse.persistance.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Classe héritant de la classe abstraite CommentStatus
 * 
 * @author Derieu
 *
 */
@Entity
@DiscriminatorValue("USEFULL")
public class CommentUsefull extends CommentStatus {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public CommentUsefull() {

	}

	/**
	 * @param comment
	 */
	public CommentUsefull(Comment comment) {
		super(comment);
	}

}
