package com.ginius.jbosse.persistance.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Classe héritant de la classe abstraite CommentStatus
 * 
 * @author Derieu
 *
 */
@Entity
@DiscriminatorValue("USELESS")
public class CommentUseless extends CommentStatus {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public CommentUseless() {

	}

	/**
	 * @param comment
	 */
	public CommentUseless(Comment comment) {
		super(comment);
	}

}
