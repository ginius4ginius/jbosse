package com.ginius.jbosse.persistance.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ginius.jbosse.persistance.entities.AnecdoteCategory;

public interface AnecdoteCategoryRepository extends JpaRepository<AnecdoteCategory, Integer> {

}
