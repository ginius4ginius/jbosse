package com.ginius.jbosse.persistance.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ginius.jbosse.persistance.entities.StatusFun;

public interface StatusFunRepository extends JpaRepository<StatusFun, Integer> {

}
