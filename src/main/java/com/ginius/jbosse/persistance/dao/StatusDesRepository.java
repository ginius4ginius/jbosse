package com.ginius.jbosse.persistance.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ginius.jbosse.persistance.entities.StatusDes;

public interface StatusDesRepository extends JpaRepository<StatusDes, Integer> {

}
