package com.ginius.jbosse.persistance.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ginius.jbosse.persistance.entities.Anecdote;

public interface AnecdoteRepository extends JpaRepository<Anecdote, Integer> {

	/**
	 * Méthode qui retourne une page des 10 meilleures anecdotes
	 * @param pageable
	 * @return
	 */
	@Query("select x from Anecdote x order by x.id") // requete à modifier
	public List<Anecdote> bestAnecdoteList();
	
	/**
	 * Méthode qui retourne une page des 10 plus récentes anecdotes
	 * @param pageable
	 * @return
	 */
	@Query("select x from Anecdote x order by x.publishedAt desc")
	public List<Anecdote> lastAnecdoteList();

	/**
	 * Méthode qui retourne une page des anecdotes par catégorie
	 * @param pageable
	 * @return
	 */
	@Query("select x from Anecdote x where x.category.id =:num order by x.publishedAt desc")
	public Page<Anecdote> anecdoteByCategoryList(@Param("num") int categoryId, Pageable pageable);

}
