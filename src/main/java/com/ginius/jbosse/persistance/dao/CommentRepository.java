package com.ginius.jbosse.persistance.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ginius.jbosse.persistance.entities.Comment;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
	
	/**
	 * Méthode qui retourne une page de commentaires d'un anecdote
	 * @param anecdoteId
	 * @param pageable
	 * @return
	 */
	@Query("select x from Comment x where x.anecdote.id = :num")
	public Page<Comment> commentsList(@Param("num")int anecdoteId, Pageable pageable);

}
