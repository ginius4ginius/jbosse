package com.ginius.jbosse.persistance.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ginius.jbosse.persistance.entities.StatusSad;

public interface StatusSadRepository extends JpaRepository<StatusSad, Integer> {

}
