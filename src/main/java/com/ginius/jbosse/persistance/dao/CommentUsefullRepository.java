package com.ginius.jbosse.persistance.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ginius.jbosse.persistance.entities.CommentUsefull;

public interface CommentUsefullRepository extends JpaRepository<CommentUsefull, Integer> {

}
