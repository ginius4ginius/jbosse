package com.ginius.jbosse.persistance.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ginius.jbosse.persistance.entities.Roles;

public interface RolesRepository extends JpaRepository<Roles, String>{

}
