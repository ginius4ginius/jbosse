package com.ginius.jbosse.persistance.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ginius.jbosse.persistance.entities.User;

public interface UserRepository extends JpaRepository<User, String> {

	@Query("select u from User u where u.email = :email")
	public User findByEmail(@Param("email") String email);

}
