package com.ginius.jbosse.persistance.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ginius.jbosse.persistance.entities.CommentUseless;

public interface CommentUselessRepository extends JpaRepository<CommentUseless, Integer> {

}
