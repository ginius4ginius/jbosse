package com.ginius.jbosse.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ginius.jbosse.persistance.entities.User;
import com.ginius.jbosse.validation.annotations.EmailMatches;

public class EmailMatchesValidator implements ConstraintValidator<EmailMatches, Object> {

	public void initialize(final EmailMatches constraintAnnotation) {
		
	}

	@Override
	public boolean isValid(final Object obj, final ConstraintValidatorContext context) {
		final User user = (User) obj;
		return user.getEmail().equals(user.getMatchingEmail());
	}
}