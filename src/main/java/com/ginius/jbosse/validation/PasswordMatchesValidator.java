package com.ginius.jbosse.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ginius.jbosse.persistance.entities.User;
import com.ginius.jbosse.validation.annotations.PasswordMatches;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

	
	@Override
	public void initialize(final PasswordMatches constraintAnnotation) {
	}

	@Override
	public boolean isValid(final Object obj, final ConstraintValidatorContext context) {
		final User user = (User) obj;
		return user.getPassword().equals(user.getMatchingPassword());
	}
}
